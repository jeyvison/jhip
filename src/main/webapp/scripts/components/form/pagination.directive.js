/* globals $ */
'use strict';

angular.module('gcmApp')
    .directive('gcmAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
