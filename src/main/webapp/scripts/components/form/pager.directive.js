/* globals $ */
'use strict';

angular.module('gcmApp')
    .directive('gcmAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
