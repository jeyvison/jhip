'use strict';

angular.module('gcmApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
