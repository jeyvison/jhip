/**
 * Data Access Objects used by WebSocket services.
 */
package com.mv.gcm.web.websocket.dto;
