/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.mv.gcm.web.rest.dto;
